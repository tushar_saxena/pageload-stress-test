#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Title         : pageload-stress-test.py
# Author        : Tushar Saxena (tushar.saxena@gmail.com)
# Date          : 07-Nov-2014
# Description   : Pageload Stress Test
# -----------------------------------------------------------------------------
# TODO: Accept USERNAME and PASSWORD from command line
# TODO: Exception handling
# TODO: Lock
# TODO: Comments
# TODO: Run pylint

# -------------------------------------
# Imports
# -------------------------------------
import json
import os
import uuid

from logger import Logger
from optparse import OptionParser
from pageload import Pageload
from statshelper import *
from threadpool import *

# -------------------------------------
# Globals
# -------------------------------------
APP_NAME = 'pageload-stress-test'
UUID = uuid.uuid4()
DEFAULT_PULSE_DURATION = 5
DEFAULT_NUMBER_OF_PULSES = 5
DEFAULT_REQUESTS_PER_PULSE = 5
DEFAULT_MAX_NUM_THREADS = 100
DEFAULT_CONFIG_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config', 'config.json')
PERCENTILE_VALUES = [0.50, 0.80, 0.90, 0.95, 0.99]
# LOCK_TIMEOUT = 60
# LOCK_NAME = APP_NAME
# LOCK_DIR = 'locks'
# LOCK_TIMEOUT = 60

# -------------------------------------
def parse_options():
	# -------------------------------------

	usage = 'Usage: %prog [options]'
	parser = OptionParser(usage)

	parser.add_option('-c', '--config', action='store', dest='config', default=DEFAULT_CONFIG_FILE, help='Configuration file to use [default=%default]')
	parser.add_option('-v', '--verbose', action='store_true', dest='verbose', default=False, help='Show verbose output [default=%default]')
	parser.add_option('-d', '--debug', action='store_true', dest='debug', default=False, help='Show debugging output [default=%default]')
	# parser.add_option('-f', '--force', action='store_true', dest='force', default=False, help='Force processing (ignore lock file). Kills locked processes [default=%default]')

	(options, args) = parser.parse_args()

	if len(args):
		parser.print_help()
		sys.exit(1)

	return options

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------
	# Parse command line args
	options = parse_options()

	"""
	# Lock handling
	lock = LockHelper(uuid=UUID, lock_dir=options.lock_dir, lock_name=options.lock_name, force=options.force, verbose=options.verbose, timeout=LOCK_TIMEOUT)

	try:
		# Acquire lock
		if lock.acquire():
			# Execute command
			lock.execute(options.command)
		else:
			# Exit with error code 1
			sys.exit(1)
	except Exception:
		# Exit with error code 2
		print traceback.format_exc()
		sys.exit(2)
	finally:
		# Release lock
		if lock.is_acquired():
			lock.release()
	"""

	# Initialize logger
	logger = Logger(name='test', debug=options.debug, verbose=options.verbose)

	# Load config file
	if os.path.exists(options.config):
		# Load config file
		with open(options.config) as json_data_file:
			config = json.load(json_data_file)
		log_msg = 'Loaded config file <%s>' % (options.config)
		logger.logf(level=logger.INFO, module='pageload-stress-test', operation='config', msg=log_msg)
	else:
		log_msg = 'Config file <%s> does not exist. Exiting ...' % (options.config)
		logger.logf(level=logger.INFO, module='pageload-stress-test', operation='config', msg=log_msg)
		sys.exit(1)

	try:
		# Initialize objects
		pulse_duration = int(config['setup']['pulse_duration']) if 'setup' in config and 'pulse_duration' in config['setup'] else DEFAULT_PULSE_DURATION
		number_of_pulses = int(config['setup']['number_of_pulses']) if 'setup' in config and 'number_of_pulses' in config['setup'] else DEFAULT_NUMBER_OF_PULSES
		requests_per_pulse = int(config['setup']['requests_per_pulse']) if 'setup' in config and 'requests_per_pulse' in config['setup'] else DEFAULT_REQUESTS_PER_PULSE
		max_number_of_threads = int(config['setup']['max_number_of_threads']) if 'setup' in config and 'max_number_of_threads' in config['setup'] else DEFAULT_MAX_NUM_THREADS
		num_threads = min(max_number_of_threads, (number_of_pulses * requests_per_pulse))

		threadpool = ThreadPool(num_threads)
		log_msg = 'Initializing threadpool with %d threads' % (num_threads)
		logger.logf(level=logger.INFO, module='pageload-stress-test', operation='threadpool', msg=log_msg)

		pageload = Pageload(logger=logger)

		timing_dict = dict()
		config_entry = config['pageload']
		config_entry['timing_dict'] = timing_dict

		# Check if login required
		login_response = pageload.login(**config_entry)
		if login_response:
			# Iterate through number of pulses
			for pulse_counter in range(number_of_pulses):
				for request_per_pulse_counter in range(requests_per_pulse):
					iteration = (pulse_counter * requests_per_pulse) + request_per_pulse_counter
					config_entry['iteration'] = iteration
					threadpool.enqueue(pageload.execute, **config_entry)
				time.sleep(pulse_duration)
		else:
			pass

		# Wait for threads to complete
		threadpool.wait()

		# Calculate Results
		timing_list = sorted(filter(lambda x: x is not None, timing_dict.values()))
		results = dict()
		results['average'] = average(timing_list)
		results['stddev'] = stddev(timing_list)
		results['percentile'] = dict()
		for percentile_value in PERCENTILE_VALUES:
			results['percentile'][percentile_value] = percentile(timing_list, percentile_value)

		# Display results
		log_msg = 'Average            : %0.05f seconds' % results['average']
		logger.logf(level=logger.INFO, module='pageload-stress-test', operation='results', msg=log_msg, verbose=True)
		log_msg = 'Standard Deviation : %0.05f seconds' % results['stddev']
		logger.logf(level=logger.INFO, module='pageload-stress-test', operation='results', msg=log_msg, verbose=True)
		for percentile_value in PERCENTILE_VALUES:
			log_msg = '%02d Percentile      : %0.05f seconds' % (percentile_value * 100, results['percentile'][percentile_value])
			logger.logf(level=logger.INFO, module='pageload-stress-test', operation='results', msg=log_msg, verbose=True)

	except Exception as _:
		exc_type, exc_value, exc_traceback = sys.exc_info()
		log_msg = 'Exception = %s :: Stacktrace = %s' % (traceback.format_exception_only(exc_type, exc_value)[0][:-1], traceback.extract_tb(exc_traceback))
		logger.logf(level=logger.ERROR, module='pageload-stress-test', operation='main', msg=log_msg)

	# Exit
	sys.exit(0)
