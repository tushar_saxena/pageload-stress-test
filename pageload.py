#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Title         : pageload.py
# Author        : Tushar Saxena (tushar.saxena@gmail.com)
# Date          : 07-Nov-2014
# Description   : Pageload Stress Test - Pageload Handler
# -----------------------------------------------------------------------------

# -------------------------------------
# Imports
# -------------------------------------
import re
import sys
import time
import traceback

from requests import session, codes

# -------------------------------------
# Globals
# -------------------------------------
DEFAULT_URL = 'http://www.google.com'

# -------------------------------------
class Pageload:
	# -------------------------------------
	logger = None
	sesion = None

	# -------------------------------------
	def __init__(self, logger):
		# -------------------------------------
		self.logger = logger
		self.session = None
		self.__init_session__()

	# -------------------------------------
	def __init_session__(self):
		# -------------------------------------
		self.session = session()
		self.session.cookies.clear_session_cookies()

	# -------------------------------------
	def login(self, url=DEFAULT_URL, login_required=False, login_url=None, login_action=None, username=None, password=None, csrf_required=False, csrf_payload_name=None, csrf_regex=None, iteration=None, timing_dict=None):
		# -------------------------------------
		if login_required:
			csrf_token = None
			if csrf_required:
				response_code, execution_time, csrf_token = self.__get_csrf_token(self.session, login_url, csrf_regex)
				if response_code == codes.ok:
					log_msg = 'GET_CSRF_TOKEN = OK :: Response Code = %s :: Execution Time = %s' % (response_code, execution_time)
					self.logger.logf(level=self.logger.DEBUG, module='pageload', operation='login', msg=log_msg)
					pass
				elif csrf_token is None:
					log_msg = 'GET_CSRF_TOKEN = CSRF Token Not Found :: Response Code = %s :: Execution Time = %s' % (response_code, execution_time)
					self.logger.logf(level=self.logger.ERROR, module='pageload', operation='login', msg=log_msg)
					return False
				else:
					log_msg = 'GET_CSRF_TOKEN = NOK :: Response Code = %s :: Execution Time = %s' % (response_code, execution_time)
					self.logger.logf(level=self.logger.ERROR, module='pageload', operation='login', msg=log_msg)
					return False
			response_code, execution_time = self.__post_login(self.session, login_url, login_action, username, password, csrf_payload_name, csrf_token)
			if response_code == codes.ok:
				log_msg = 'POST_LOGIN = OK :: Response Code = %s :: Execution Time = %s' % (response_code, execution_time)
				self.logger.logf(level=self.logger.DEBUG, module='pageload', operation='login', msg=log_msg)
				pass
			else:
				log_msg = 'POST_LOGIN = NOK :: Response Code = %s :: Execution Time = %s' % (response_code, execution_time)
				self.logger.logf(level=self.logger.ERROR, module='pageload', operation='login', msg=log_msg)
				return False
		else:
			log_msg = 'Login not required'
			self.logger.logf(level=self.logger.INFO, module='pageload', operation='login', msg=log_msg)

		log_msg = 'Login successful'
		self.logger.logf(level=self.logger.INFO, module='pageload', operation='login', msg=log_msg)
		return True

	# -------------------------------------
	def execute(self, url=DEFAULT_URL, login_required=False, login_url=None, login_action=None, username=None, password=None, csrf_required=False, csrf_payload_name=None, csrf_regex=None, iteration=None, timing_dict=None):
		# -------------------------------------
		try:
			response_code, execution_time = self.__get_webpage(self.session, url)
			if response_code == codes.ok:
				log_msg = 'Iteration = %05d :: GET_WEBPAGE GET = OK :: Response Code = %s :: Execution Time = %s' % (iteration, response_code, execution_time)
				self.logger.logf(level=self.logger.DEBUG, module='pageload', operation='execute', msg=log_msg)
				log_msg = '[%05d] %0.05f seconds' % (iteration, execution_time)
				self.logger.logf(level=self.logger.INFO, module='pageload', operation='execute', msg=log_msg, verbose=True)
				timing_dict[iteration] = execution_time
				return
			else:
				log_msg = 'Iteration = %05d :: GET_WEBPAGE GET = NOK :: Response Code = %s :: Execution Time = %s' % (iteration, response_code, execution_time)
				self.logger.logf(level=self.logger.ERROR, module='pageload', operation='execute', msg=log_msg)
				timing_dict[iteration] = None
				return
		except Exception as _:
			exc_type, exc_value, exc_traceback = sys.exc_info()
			log_msg = 'Exception = %s :: Stacktrace = %s' % (traceback.format_exception_only(exc_type, exc_value)[0][:-1], traceback.extract_tb(exc_traceback))
			self.logger.logf(level=self.logger.ERROR, module='pageload', operation='execute', msg=log_msg)
			timing_dict[iteration] = None

	# -------------------------------------
	def __get_csrf_token(self, session, login_url, csrf_regex):
		# -------------------------------------
		try:
			t_start = time.time()
			response = self.session.get(login_url)
			match = re.search(csrf_regex, response.text)
			csrf_token = match.group(1) if match else None
			execution_time = time.time() - t_start
			return response.status_code, execution_time, csrf_token
		except Exception as _:
			exc_type, exc_value, exc_traceback = sys.exc_info()
			log_msg = 'Exception = %s :: Stacktrace = %s' % (traceback.format_exception_only(exc_type, exc_value)[0][:-1], traceback.extract_tb(exc_traceback))
			self.logger.logf(level=self.logger.ERROR, module='pageload', operation='execute', msg=log_msg)
			return None, None, None

	# -------------------------------------
	def __post_login(self, session, login_url=None, login_action=None, username=None, password=None, csrf_payload_name=None, csrf_token=None):
		# -------------------------------------
		try:
			# Build payload
			payload = {
			'action': login_action,
			'username': username,
			'password': password,
			}
			if csrf_token:
				payload[csrf_payload_name] = csrf_token
			# Submit POST Request
			t_start = time.time()
			response = self.session.post(login_url, data=payload)
			execution_time = time.time() - t_start
			return response.status_code, execution_time
		except Exception as _:
			exc_type, exc_value, exc_traceback = sys.exc_info()
			log_msg = 'Exception = %s :: Stacktrace = %s' % (traceback.format_exception_only(exc_type, exc_value)[0][:-1], traceback.extract_tb(exc_traceback))
			self.logger.logf(level=self.logger.ERROR, module='pageload', operation='execute', msg=log_msg)
			return None, None, None

	# -------------------------------------
	def __get_webpage(self, session, url):
		# -------------------------------------
		try:
			t_start = time.time()
			response = self.session.get(url)
			execution_time = time.time() - t_start
			return response.status_code, execution_time
		except Exception as _:
			exc_type, exc_value, exc_traceback = sys.exc_info()
			log_msg = 'Exception = %s :: Stacktrace = %s' % (traceback.format_exception_only(exc_type, exc_value)[0][:-1], traceback.extract_tb(exc_traceback))
			self.logger.logf(level=self.logger.ERROR, module='pageload', operation='execute', msg=log_msg)
			return None, None

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------
	print "Cannot execute this script as a standalone program"
