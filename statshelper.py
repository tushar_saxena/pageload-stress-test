#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Title         : statshelper.py
# Author        : Tushar Saxena (tushar.saxena@gmail.com)
# Date          : 07-Nov-2014
# Description   : Pageload Stress Test - Stats helper
# http://code.activestate.com/recipes/511478-finding-the-percentile-of-the-values/
# http://calebmadrigal.com/standard-deviation-in-python/
# -----------------------------------------------------------------------------

# -------------------------------------
# Imports
# -------------------------------------
import math

# -------------------------------------
def percentile(N, percent, key=lambda x: x):
	# -------------------------------------
	"""
	Find the percentile of a list of values.

	@parameter N - is a list of values. Note N MUST BE already sorted.
	@parameter percent - a float value from 0.0 to 1.0.
	@parameter key - optional key function to compute value from each element of N.

	@return - the percentile of the values
	"""
	if not N:
		return None
	k = (len(N) - 1) * percent
	f = math.floor(k)
	c = math.ceil(k)
	if f == c:
		return key(N[int(k)])
	d0 = key(N[int(f)]) * (c - k)
	d1 = key(N[int(c)]) * (k - f)
	return d0 + d1

# -------------------------------------
# AVERAGE, VARIANCE and STDDEV
# -------------------------------------
average = lambda x: sum(x) * 1.0 / len(x)
variance = lambda x: map(lambda y: (y - average(x)) ** 2, x)
stddev = lambda x: math.sqrt(average(variance(x)))

