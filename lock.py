#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Title         : lockhelper.py
# Author        : Tushar Saxena (tushar.saxena@taxiforsure.com)
# Date          : 03-Sep-2014
# Description   : Python lock helper
# -----------------------------------------------------------------------------

# -------------------------------------
# Imports
# -------------------------------------
import os
import time
import re
import signal
import json
import subprocess

# -------------------------------------
# Globals
# -------------------------------------
LOCK_DIR = 'locks'
LOCK_SUFFIX = '.pid'

# -------------------------------------
class LockHelper:
	# -------------------------------------
	uuid = None
	lock_dir = None
	lock_name = None
	lock_file = None
	force = None
	verbose = None
	timeout = None

	lock_regex = None
	lock_list = None

	APP_NAME = 'lock'

	# -------------------------------------
	def __init__(self, uuid=None, lock_dir=None, lock_name=None, force=False, verbose=False, timeout=None):
		# -------------------------------------
		# Init
		self.uuid = str(uuid)
		self.lock_dir = os.path.abspath(lock_dir if lock_dir else LOCK_DIR)
		self.lock_name = lock_name
		self.force = force
		self.verbose = verbose
		self.timeout = timeout

		# Build file pattern match
		self.lock_regex = re.compile(self.lock_name)

		# Check if lock_dir exists, create if it doesn't
		if not os.path.isdir(self.lock_dir):
			os.makedirs(self.lock_dir)

	# -------------------------------------
	def acquire(self):
		# -------------------------------------
		# Get a list of all lock_files in lock_dir
		old_lock_file = None
		old_lock_file_timestamp = 0.0
		for file in os.listdir(self.lock_dir):
			# Check if current filename matches the regex pattern
			if self.lock_regex.match(file):
				try:
					current_file = os.path.join(self.lock_dir, file)
					(parent_pid, timestamp, child_pid) = self.read_lock_file(current_file)
					if timestamp > old_lock_file_timestamp:
						old_lock_file = file
						old_lock_file_timestamp = timestamp
				except Exception as _:
					pass

		# Check if old_lock_file exists
		if old_lock_file and os.path.exists(os.path.join(self.lock_dir, old_lock_file)):
			# lock_file exists, check if --force has been specified
			if self.force:
				# --force, acquire lock
				LOG_MSG = {'mode': 'ACQUIRE', 'msg': 'lock_file exists, overriding with --force : %s' % old_lock_file}
				self.log_message(LOG_MSG)
				return self.__acquire_lock()
			else:
				# No --force, check for timeout
				time_diff = time.time() - old_lock_file_timestamp
				if time_diff > self.timeout:
					# Timeout has happened, acquire lock
					LOG_MSG = {'mode': 'ACQUIRE', 'msg': 'lock_file exists, OLDER than timeout value (%d seconds) : %s - %d seconds old. Acquring lock.' % (self.timeout, old_lock_file, time_diff)}
					self.log_message(LOG_MSG)
					return self.__acquire_lock()
				else:
					# No timeout, don't acquire lock
					LOG_MSG = {'mode': 'ACQUIRE', 'msg': 'lock_file exists, NEWER than timeout value (%d seconds) : %s - %d seconds old. Not acquiring lock. Override with --force' % (self.timeout, old_lock_file, time_diff)}
					self.log_message(LOG_MSG)
					return False
		else:
			# lock_file does not exist, acquire lock
			return self.__acquire_lock()

	# -------------------------------------
	def __acquire_lock(self):
		# -------------------------------------
		# Kill all existing processes and delete lock_files
		if self.force:
			for file in filter(lambda x: self.lock_regex.match(x), os.listdir(self.lock_dir)):
				# Read lock file
				lock_file = os.path.join(self.lock_dir, file)
				(parent_pid, timestamp, child_pid) = self.read_lock_file(lock_file)
				# Terminate PIDs
				self.terminate_process(child_pid)
				self.terminate_process(parent_pid)

		# Create new lock_file
		try:
			lock_filename = '%s.%s' % (self.lock_name, os.getpid())
			lock_file = os.path.join(self.lock_dir, lock_filename)
			self.lock_file = lock_file
			self.write_lock_file(lock_file, mode='w', timestamp=time.time(), child=None)
			LOG_MSG = {'mode': 'ACQUIRE', 'msg': lock_file}
			self.log_message(LOG_MSG)
		except Exception as _:
			LOG_MSG = {'mode': 'ACQUIRE', 'msg': 'Error creating new lock_file : %s : %s' % (lock_file, e)}
			self.log_message(LOG_MSG, override=True)
			return False

		return True

	# -------------------------------------
	def execute(self, command):
		# -------------------------------------
		# Spawn new process to execute command
		LOG_MSG = {'mode': 'EXECUTE', 'msg': command}
		self.log_message(LOG_MSG)
		p = subprocess.Popen(command.split())

		# Add child entry to lock_file
		try:
			self.write_lock_file(self.lock_file, mode='a', timestamp=None, child=p.pid)
			LOG_MSG = {'mode': 'ADD_CHILD_PID', 'msg': 'Added Child PID %d to lockfile %s' % (p.pid, self.lock_file)}
			self.log_message(LOG_MSG)
		except Exception as _:
			LOG_MSG = {'mode': 'ADD_CHILD_PID', 'msg': 'Error updating lock_file : %s : %s' % (self.lock_file, e)}
			self.log_message(LOG_MSG, override=True)

		# Wait for process to end
		p.communicate()

		# Display Return Code
		LOG_MSG = {'mode': 'RETURN_CODE', 'msg': p.returncode}
		self.log_message(LOG_MSG)

	# -------------------------------------
	def release(self):
		# -------------------------------------
		# Check is lock exists
		if self.is_acquired():
			# Exists, release lock
			try:
				os.remove(self.lock_file)
				LOG_MSG = {'mode': 'RELEASE', 'msg': '%s' % self.lock_file}
				self.log_message(LOG_MSG)
			except Exception as _:
				LOG_MSG = {'mode': 'RELEASE', 'msg': 'Error releasing lock file %s : %s' % (self.lock_file, e)}
				self.log_message(LOG_MSG)
				return False
		else:
			# Doesn't exist
			LOG_MSG = {'mode': 'RELEASE', 'msg': 'lock file does not exist : %s' % self.lock_file}
			self.log_message(LOG_MSG)
			return False

		return True

	# -------------------------------------
	def read_lock_file(self, filename):
		# -------------------------------------
		parent_pid = timestamp = child_pid = None

		# Read lock file
		with open(filename, 'r') as f:
			lines = f.readlines()

		# Get parent PID
		parent_pid = int(filename.split('.')[-1])

		# Get timestamp
		if len(lines) >= 1:
			timestamp = float(lines[0][:-1].split('=')[1])

		# Get child PID
		if len(lines) >= 2:
			child_pid = int(lines[1][:-1].split('=')[1])

		return parent_pid, timestamp, child_pid

	# -------------------------------------
	def write_lock_file(self, filename, mode='w', timestamp=None, child=None):
		# -------------------------------------
		with open(filename, mode) as f:
			# Write timestamp
			if timestamp:
				f.write('timestamp=%f\n' % timestamp)
			# Write child
			if child:
				f.write('child=%d\n' % child)

	# -------------------------------------
	def is_process_running(self, pid):
		# -------------------------------------
		try:
			# Send Signal 0
			os.kill(pid, 0)
			# No exception, PID is running
			return True
		except OSError:
			# Exception thrown, PID not running
			return False

	# -------------------------------------
	def terminate_process(self, pid):
		# -------------------------------------
		try:
			# Terminate process
			if self.is_process_running(pid):
				os.kill(pid, signal.SIGKILL)
				LOG_MSG = {'mode': 'TERMINATE', 'msg': 'Killed process with pid : %d' % pid}
				self.log_message(LOG_MSG)
		except Exception as _:
			pass

	# -------------------------------------
	def is_acquired(self):
		# -------------------------------------
		if self.lock_file > 0:
			return True
		else:
			return False

	# -------------------------------------
	def log_message(self, message_dict, override=False):
		# -------------------------------------
		if self.verbose or override:
			message_dict['app_name'] = self.APP_NAME
			message_dict['id'] = self.uuid
			message_dict['_timestamp'] = time.strftime('%Y/%m/%d %H:%M:%S %Z')
			print json.dumps(message_dict, sort_keys=True)

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------
	print 'Error : This python script cannot be run as a standalone program.'
