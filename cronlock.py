#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Title         : cronlock.py
# Author        : Tushar Saxena (tushar.saxena@taxiforsure.com)
# Date          : 03-Sep-2014
# Description   : Python lock file wrapper for cron_lock
# -----------------------------------------------------------------------------

# -------------------------------------
# Imports
# -------------------------------------
import sys
import uuid
import traceback
from optparse import OptionParser
from lockhelper import *

# -------------------------------------
# Globals
# -------------------------------------
APP_NAME = 'lockwrapper'
UUID = uuid.uuid4()
LOCK_TIMEOUT = 60
LOCK_DIR = 'locks'

# -------------------------------------
def parseOptions():
	# -------------------------------------

	usage = 'Usage: %prog [options]'
	parser = OptionParser(usage)

	parser.add_option('-l', '--lockname', action='store', dest='lock_name', default=None, help='Lock name to check/set for current command set [mandatory]')
	parser.add_option('-c', '--command', action='store', dest='command', default=None, help='Command to execute [mandatory]')
	parser.add_option('-d', '--lockdir', action='store', dest='lock_dir', default=LOCK_DIR, help='Directory to use for lock files [default=%default]')
	parser.add_option('-t', '--timeout', action='store', dest='timeout', default=LOCK_TIMEOUT, help='Lock timeout value in seconds [default=%default]')
	parser.add_option('-f', '--force', action='store_true', dest='force', default=False, help='Force processing (ignore lock file). Kills locked processes [default=%default]')
	parser.add_option('-v', '--verbose', action='store_true', dest='verbose', default=False, help='Show verbose output [default=%default]')

	(options, args) = parser.parse_args()

	if len(args):
		parser.print_help()
		sys.exit(1)

	if not options.command:
		print 'No command specified (-c or --command COMMAND)'
		parser.print_help()
		sys.exit(1)

	if not options.lock_name:
		print 'No lock name specified (-l or --lock LOCK)'
		parser.print_help()
		sys.exit(1)

	return options

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------

	# Parse command line args
	options = parseOptions()

	# Lock handling
	lock = LockHelper(uuid=UUID, lock_dir=options.lock_dir, lock_name=options.lock_name, force=options.force, verbose=options.verbose, timeout=LOCK_TIMEOUT)

	try:
		# Acquire lock
		if lock.acquire():
			# Execute command
			lock.execute(options.command)
		else:
			# Exit with error code 1
			sys.exit(1)
	except Exception:
		# Exit with error code 2
		print traceback.format_exc()
		sys.exit(2)
	finally:
		# Release lock
		if lock.is_acquired():
			lock.release()

	# Exit
	sys.exit(0)
