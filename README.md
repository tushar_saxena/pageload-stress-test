# *Pageload Stress Test README*

## Introduction

Pageload Stress Test python script which enables you to stress test a webpage by executing multiple page loads from a single machine using multiple threads. It allows you to specify a username and password to use to login to the site, and even takes care of CSRF tokens if required for your site.

## Configuring Pageload Stress Test

Take a look at the default configuration files in the config folder. They are fairly self-explanatory, so just play around with the various options. Please note that the configuration files are written in strict JSON notation, so use a JSON validator like [JSONLint](http://jsonlint.com) to validate your configuration files.

A sample configuration file:

    {
        "setup": {
            "pulse_duration": 5,
            "requests_per_pulse": 5,
    	    "number_of_pulses": 2,
    	    "max_number_of_threads": 100
    	},
        "pageload": {
            "url": "http://website.to.test.com/",
            "login_required": true,
            "login_url": "http://website.to.test.com/accounts/login/",
            "login_action": "/accounts/login/",
            "username": "username",
            "password": "password",
            "csrf_required": true,
            "csrf_payload_name": "csrftoken",
            "csrf_regex": "input type='hidden' name='csrftoken' value='(.+?)'"
        }
    }

## Executing Pageload Stress Test

    $ python pageload-stress-test.py --help
    Usage: pageload-stress-test.py [options]

    Options:
      -h, --help            show this help message and exit
      -c CONFIG, --config=CONFIG
                            Configuration file to use
                            [default=/home/username/host/GIT/personal
                            /pageload-stress-test/config/config.json]
      -v, --verbose         Show verbose output [default=False]
      -d, --debug           Show debugging output [default=False]

## Sample Output

    [00002] 0.85403 seconds
    [00003] 0.91559 seconds
    [00000] 0.94822 seconds
    [00004] 1.16185 seconds
    [00001] 3.55900 seconds
    [00006] 0.67431 seconds
    [00009] 0.87308 seconds
    [00007] 3.79182 seconds
    [00008] 4.29498 seconds
    [00005] 6.86829 seconds
    Average            : 2.39412 seconds
    Standard Deviation : 2.00949 seconds
    50 Percentile      : 1.05504 seconds
    80 Percentile      : 3.89246 seconds
    90 Percentile      : 4.55231 seconds
    95 Percentile      : 5.71030 seconds
    99 Percentile      : 6.63670 seconds